# Team Members

* Jadin Casey
* Rial Johnson
* Stuart Dilts

# Idea 1 - How to convert an ER diagram to a Relationship diagram
This tutorial explains the concept of relationship diagrams and then goes on to show methods for converting an ER diagram into a relational one. By the end the reader should have a solid understanding of what a relational diagram is, have the toolset for constructing relational diagrams, and have some intuition on the best ways to represent a data type in a relational format. 

# Idea 2 - The basics of sqlite 
In this tutorial we would introduce a reader to sqlite beginning with basic commands for viewing and constructing a database and then moving on to manipulating and querying data. Towards the end of the tutorial we’d show how .sql files work and the usefulness of them. After completing this tutorial a person would understand what sqlite is and have the basic knowledge on how to view the data and manipulate it. The tutorial could also hopefully serve as reference material to look back on while working on a database.

# Idea 3 - How to use Join Statements in SQL
This tutorial would introduce Join statements in SQL. What is a cartesian product? Why would we not want to use the entire cartesian product in our queries? How do we filter from this cartesian product to get the query we want? What does it mean to Inner Join, Outer Join, Full Join, Right Join, Left Join, and Self Join? When do we want to Join instead of selecting from multiple tables? How do we use Join statements with other statements to get the data we want? By the ender of the tutorial, the user should understand how using join statements can simplify their queries and lead them to desired data and how they fit into SQL querying. 

# Bonus Idea - How to create a conceptual data model for a database
This tutorial would cover the basics of ER diagrams (entities, attributes, and relationships) and how they represent data. By the end of the tutorial the reader would understand how to identify an entity, where to put attributes, and what relations to add in a simple ER diagram. We hope for this tutorial to get a reader’s foot wet with ER diagrams so that they understand how to read one and can begin constructing their own.
